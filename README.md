# 3rd party demo app for AgentCASH

## Introduction

This repository contains simple demo application which demonstrates the usage of AgentCASH for payments from 3rd party application. 

*Note: while this application demonstrates the payment using the AgentCASH's charge intent, there exists alternative way to use AgentCASH SDK.*

This project contains all source code necessary to initiate payment and receive response from the AgentCASH app once payment has been executed.

Access the complete documentation [here](AgentCASH-MobileApp-IntegrationSpec.pdf).

Visit us at https://www.agentcash.com to learn more about what we do.

## Prerequisites

Before trying out the application, download the latest version of AgentCASH application from Google store. Run the application and log in as existing user, or sign in as new merchant. Alternatively, a new merchant can be created directly from the web admin interface at https://www.agentcash.com, and use that data to log in into AgentCASH application.

*Note: Demo application will always start payment as the last user which is logged in in the AgentCASH application.*

## Build and run the application

Clone the repository
  
Open the project in Android Studio

Build and run the application. Enter the amount to charge and press charge button.

## License

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.