package com.example.agentcash.acdemointent;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.agentcash.acdemointent.model.Payment;
import com.example.agentcash.acdemointent.utils.Utils;

import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final int ACTIVITY_REQUEST_CODE = 111;

    // to send data to Agent Cash payment intent
    private final String AC_PATH = "com.agentcash.register";
    private final String AC_INTENT = AC_PATH + ".action.CHARGE_PAYMENT";
    private final String AC_AMOUNT = AC_PATH + ".extra.AMOUNT";
    private final String AC_CURRENCY = AC_PATH + ".extra.CURRENCY";
    private final String AC_EXTERNAL_IDENTIFIER = AC_PATH + ".extra.EXTERNAL_IDENTIFIER";
    private final String AC_PAYMENT_TYPE_CODE = AC_PATH + ".extra.PAYMENT_TYPE_CODE";
    private final String AC_PAYMENT_CALLBACK_URL = AC_PATH + ".extra.PAYMENT_CALLBACK_URL";
    private final String AC_PARTNER_ID = AC_PATH + ".extra.PARTNER_ID";
    private final String AC_API_KEY = AC_PATH + ".extra.API_KEY";

    // parameters returning data from Agent Cash payment
    private final String AC_STATUS_MESSAGE = AC_PATH + ".extra.STATUS_MESSAGE";
    private final String AC_PAYMENT_DATA = AC_PATH + ".extra.PAYMENT_DATA";

    private EditText chargeAmountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chargeAmountView = (EditText)findViewById(R.id.chargeAmount);
    }

    public void onChargeClick(View v)
    {
        Intent acIntent = new Intent(AC_INTENT);

        // amount - always string, 2 decimal places, '.' as decimal separator
        acIntent.putExtra(AC_AMOUNT, formatAmount(chargeAmountView.getText().toString()));

        // use ISO4217 code for currency. note, merchant must support that currency
        acIntent.putExtra(AC_CURRENCY, "HRK");

        // currently credit cards are only supported, therefore 'creditCard'
        acIntent.putExtra(AC_PAYMENT_TYPE_CODE, "creditCard");

        // 3rd party string to be used to identify the transaction on 3rd party system
        acIntent.putExtra(AC_EXTERNAL_IDENTIFIER, "TRN 123-456-789");


        // set own callback (to be notified by AC server) with the payment results
        //acIntent.putExtra(AC_PAYMENT_CALLBACK_URL, url);

        // partner id is currently not used
        //acIntent.putExtra(AC_PARTNER_ID, partner_id);

        // API Key is currently not used, instead it relies on logged in user
        //acIntent.putExtra(AC_API_KEY, api_key);

        try {
            startActivityForResult(acIntent, ACTIVITY_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            // application is not installed
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_REQUEST_CODE) {
            if (data == null) {
                return;
            }

            String paymentData = data.getStringExtra(AC_PAYMENT_DATA);
            Payment payment = null;
            if (paymentData != null) {
                payment = Utils.getAPIParserGson().fromJson(paymentData, Payment.class);
            }

            switch (resultCode) {
                // result OK, payment successful
                case Activity.RESULT_OK:
                    Toast.makeText(this, "Payment success for transaction " + payment.getExternalId(), Toast.LENGTH_LONG).show();
                    break;

                // result CANCELED, payment failed
                case Activity.RESULT_CANCELED:
                    String message = data.getStringExtra(AC_STATUS_MESSAGE);

                    // payment can be returned depending whether it was canceled after payment object was created or before
                    if (payment != null) {
                        Toast.makeText(this, "Payment canceled for rrn " + payment.getRrn(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Charge canceled with message " + message, Toast.LENGTH_LONG).show();
                    }
                    break;

                // handle error
                default:
                    // payment can be returned depending whether error happened canceled after payment object was created or before
                    String errorMessage = data.getStringExtra(AC_STATUS_MESSAGE);
                    Toast.makeText(this, "Charge failed with error " + errorMessage, Toast.LENGTH_LONG).show();
            }
        }
    }


    public String formatAmount(String amount) {
        try {
            double number = Double.parseDouble(amount);
            return String.format(Locale.ENGLISH, "%.2f", number);
        } catch (NumberFormatException e) {
            return amount;
        }
    }

}
